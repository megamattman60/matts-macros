if (!actor) {
    ui.notifications.warn("You must have an actor selected.");
	
} else {

let equippedWeapons = {};
let selectedWeapon = "";

(async () => {
    for (let token of canvas.tokens.controlled) {
        const weapon = token.actor.data.items.filter(item => item.type === 'weapon');
        console.log(weapon);

        weapon.forEach(item => {
			//Create entry to be passed to dialog buttons
			equippedWeapons[item.name] = {label : item.name, callback: () => selectedWeapon = item}
			console.log(item.name)
      });
	}
	console.log(equippedWeapons);
	new Dialog({
		title: "Select Weapon",    
		buttons:  equippedWeapons
		,
		default: "Cancel",
		close: html => {
			updateWeapon(selectedWeapon)
		}
	}).render(true);

})();

function removeRule(arr, value, remove) {
	arr.forEach(function (item, index) {
		if (item) {
			if (item.label == value.label) {
				delete arr[index]
			}
		}
	});
	//clean up empty elements
	let cleanArray = arr.filter(function () { return true });
	return cleanArray		
	
}

//{"key":"PF2E.RuleElement.DamageDice","label":mwStrikingRuleLabel,"diceSize":"d6","selector":"0oAXw0gtV8URtAvT-damage","value":{"brackets":[{"value":{"diceNumber":1}}]}}

async function updateWeapon(item) {
	// build list of selected players ids for whispers target
	  if (!item) {
		  return false
	  }
	  let mwAttackBonusRuleLabel = "Magic Weapon Attack";
	  let mwStrikingRuleLabel = "Magic Weapon Striking";
	  
	  let mwAttackBonusSelect = item._id + "-attack"
	  let mwStrikeSelector = item._id + "-damage"
	
	  let mwStrikingRule = {"key":"PF2E.RuleElement.DamageDice", "label":mwStrikingRuleLabel,"value":"1", "dieSize":item.data.damage.die, "selector":mwStrikeSelector, "value":{"brackets":[{"value":{"diceNumber":1}}]}}	  
	  let mwAttackBonusRule = {"key":"PF2E.RuleElement.FlatModifier","label":mwAttackBonusRuleLabel,"value":"1", "selector":mwAttackBonusSelect, "type": "item"}
	  
	  let mwAttackBonusRulePresent = item.data.rules.filter(rule=> rule.label == mwAttackBonusRuleLabel);
	  let mwStrikingRulePresent = item.data.rules.filter(rule=> rule.label == mwStrikingRuleLabel);
					  
	  if (mwAttackBonusRulePresent.length || mwStrikingRulePresent.length) {
		  // if either rules exist then MW has been cast on this weapon
		  // assume that the user want to remove the buff				
		  if (mwAttackBonusRulePresent){
			  item.data.rules = removeRule(item.data.rules, mwAttackBonusRule, true);			
		  }		  		  
		  if (mwStrikingRulePresent) {
			 item.data.rules = removeRule(item.data.rules, mwStrikingRule, true);
		  }		  
		} else {			
		    // only add striking rule if weapon has single damage dice and doesn't currenlty have striking
			if (item.data.strikingRune.value == "") {				
				item.data.rules.push(mwStrikingRule);			
			}
			item.data.rules.push(mwAttackBonusRule);			
		}		
		//show item edit screen, when it is closed the actions recalculate
		new (0, CONFIG.Item.entityClass)(actor.getOwnedItem(item._id).data,{actor: actor}).sheet.render(!0)

	}
}
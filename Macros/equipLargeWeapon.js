let keyword = "Oversized";

 for (let token of canvas.tokens.controlled) {
	let largeWeapons = {};
	let selectedWeapon = "";
	(async () => {

		//find large weapons
		const weapon = token.actor.data.items.filter(item => item.type === 'weapon').filter(weapon => weapon.name.includes(keyword));
		weapon.forEach(item => {
			//Create entry to be passed to dialog buttons
			largeWeapons[item.name] = {label : item.name, callback: () => selectedWeapon = item}
			console.log(item.name)
		})

		console.log(largeWeapons);
		let largeWeaponCount = Object.keys(largeWeapons).length
		if (largeWeaponCount == 0)
		{
			ui.notifications.warn("Actor must have a weapon with " + keyword + " in its name.");
		}
		else if (largeWeaponCount == 1) {
			equipLargeWeapon(token, weapon[0])
		}
		else (
			new Dialog({
				title: "Select Weapon",
				buttons:  largeWeapons,
				default: "Cancel",
				close: html => {
					equipLargeWeapon(token, selectedWeapon)
				}
			}).render(true)
		)
	})();
}

async function equipLargeWeapon (token, weapon) {
	// if already have this effect active, turn it off
	console.log(weapon)
	let largeWeaponEquippedEffectName = "Effect: "+ keyword +" Weapon Equipped " + weapon.name;
	let largeWeaponEffectEquippedPresent = actor.items.find(i=>i.name===largeWeaponEquippedEffectName)

	if (largeWeaponEffectEquippedPresent){
	  largeWeaponEffectEquippedPresent.delete()
	  let item = actor.items.filter(i=>i.name==="Clumsy")[0]
	  await PF2eConditionManager.removeConditionFromToken(item.data._id, token)
	  item.delete()
	} else {
	  let item = game.items.getName('Clumsy');
	  await PF2eConditionManager.addConditionToToken(item.data, token)
	  let largeWeaponEquippedEffect = {effects:[], name:largeWeaponEquippedEffectName, type:"effect"}
	  actor.createOwnedItem(largeWeaponEquippedEffect)
	}
}

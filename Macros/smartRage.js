/*
Find out if the actor has giant instinct, if it does,
then add extra rules for rage damage for large weapons

Rely on an Effect: Rage existing these are the rules for it:
{
  "key": "PF2E.RuleElement.FlatModifier",
  "selector": "damage",
  "label": "Rage",
  "predicate": {
    "not": ["agile", "oversized"]
  },
  "value": {
    "brackets": [
      { "end": 6, "value": 2 },
      { "start": 7, "end": 14, "value": 6 },
      { "start": 15, "value": 12 }
    ]
  }
}
{"key":"PF2E.RuleElement.FlatModifier","label":"Rage","predicate":{"all":["rage"]}"""selector":"ac","value":-1}
{"key":"PF2E.RuleElement.TempHP","value":"@abilities.con.mod + @details.level.value"}

For additional rules to be added for oversized weapon the item in invetory need to be name needs to include Oversized

*/

(async () => {
    if (!actor) {
        ui.notifications.warn("Please select a token");
        return false;
    }
    let hasRage = actor.data.items.filter(item => item.type === 'action').filter(action => action.name==="Rage");
    console.log(hasRage)
    if (!hasRage.length) {
        ui.notifications.warn("Please select a token that has the rage action");
        return false;
    }

    let functionMap = {
        "Animal Instinct" : applyBestialInstinct,
        "Giant Instinct" : applyGiantInstinct,
        "Spirit Instinct" : applySpiritInstinct,
        "Dragon Instinct" : applyDragonInstinct,
        "Fury Instinct" : applyFuryInstinct
    }

    //hopfully there is nothing else called Instinct
    let hasRageInstinct = actor.data.items.filter(item => item.type === 'feat').find(feat => feat.name.includes("Instinct"));
    if (hasRageInstinct) {
        console.log(`LOOKS LIKE YOU HAVE: ${hasRageInstinct.name}`)
        await functionMap[hasRageInstinct.name]()

    }
})();


async function applyFuryInstinct(){
    let rageEffect = game.items.getName("Effect: Rage Fury Instinct").data
    await actor.createOwnedItem(rageEffect)
}

async function applySpiritInstinct(){
    let rageEffect = game.items.getName("Effect: Rage Spirit Instinct").data
    await actor.createOwnedItem(rageEffect)
}

async function applyDragonInstinct(){
    let rageEffect = game.items.getName("Effect: Rage Dragon Instinct").data
    //TODO:- add damageType to damage rule
    await actor.createOwnedItem(rageEffect)
}

async function removeRageEffect () {
    //Toggle rage off if already on
    let hasRageEffect = actor.items.find(item => item.type === 'effect' && item.name.includes("Effect: Rage"));
    if (hasRageEffect){
        actor.deleteOwnedItem(hasRageEffect._id)
        return true;
    }
    return false;
}

//Giant instinct create custom rules based on equipped weapons, copy the spell effect from items directory
//Then add custom rules
async function applyGiantInstinct(){
    if (await removeRageEffect()){
        return true;
    }
    let rageEffect = game.items.getName("Effect: Rage Giant Instinct").data
    let newRageRules = [...rageEffect.data.rules]
    let largeWeapons = actor.data.items.filter(item => item.type === 'weapon').filter(weapon => weapon.name.includes("Oversized"));
    for (let largeWeapon of largeWeapons) {

        let newRageRule={"key": "PF2E.RuleElement.FlatModifier", "selector":largeWeapon._id + "-damage", "label": "Rage", "predicate": {"not": ["agile"]}, "value": {"brackets": [{ "end": 6, "value": 6 }, { "start": 7, "end": 14, "value": 10 },{ "start": 15, "value": 18 }]}}
        newRageRules.push(newRageRule)
    }
    let largeWeaponEquippedEffect = actor.data.items.filter(item => item.type === 'effect').filter(effect => effect.name.includes("Large Weapon Equipped"))
    console.log(largeWeaponEquippedEffect);
    if (!largeWeaponEquippedEffect.length) {
        ui.notifications.warn("No Large weapon effect found, set clumsy to at least 1 or apply Large weapon equipped effect");
    }
    let newRageEffect = {"data.rules": newRageRules, "data.description":rageEffect.data.description, "img": rageEffect.img, effects:[], name:"Effect: Rage Giant Instinct", type:"effect"}
    await actor.createOwnedItem(newRageEffect)
}

async function applyBestialInstinct(){
    //check if rage already present - remove natural weapons and then exit
    let keyword = "-Animal Instinct-"
    if (removeRageEffect()){
        let foundNaturalWeapons = token.actor.data.items.filter(item => item.type === 'weapon').filter(weapon => weapon.name.includes(keyword));
        if (foundNaturalWeapons.length){
            foundNaturalWeapons.forEach(foundNaturalWeapon=>actor.deleteOwnedItem(foundNaturalWeapon._id))
            return true
        }
        return true
    }

    //apply rage effect
    let rageEffect = game.items.getName("Effect: Rage Animal Instinct").data
    await actor.createOwnedItem(rageEffect)
    //Ask player their animal
    let animalChoices = {
        "Ape": [{"name":"Fist", "die":"d10", "damageType":"B", "traits": ["Grapple", "unarmed"]}],
        "Bear":[{"name":"Jaws", "die": "d10", "damageType":"piercing", "traits":[" Unarmed"]},
                {"name":"Claw", "die": "d6", "damageType": "slashing","Traits":[" Agile", "unarmed"]}],
        "Frog":[{"name":"Jaws", "die": "d10", "damageType":"B", "traits":[" Unarmed"]},
                {"name":"Tongue", "die": "d4", "damageType": "B","Traits":[" Agile", "unarmed"]}],
        "Cat":[{"name":"Jaws", "die": "d10", "damageType":"piercing", "traits":[" Unarmed"]},
                {"name":"Claw", "die": "d6", "damageType": "slashing","Traits":[" Agile", "unarmed"]}],
        "Bull": [{"name":"Horn", "die":"d10", "damageType":"piercing", "traits": ["Shove", "unarmed"]}],
        "Deer": [{"name":"Antler", "die":"d10", "damageType":"piercing", "traits": ["Grapple", "unarmed"]}],
        "Shark": [{"name":"Jaws", "die":"d10", "damageType":"piercing", "traits": ["Grapple", "unarmed"]}],
        "Snake": [{"name":"Fangs", "die":"d10", "damageType":"piercing", "traits": ["Grapple", "unarmed"]}],
        "Wolf": [{"name":"Fangs", "die":"d10", "damageType":"piercing", "traits": ["Trip", "unarmed"]}]
    }
    let animalChoice = ""
    let animalButtons = {}
    for (let animal of Object.keys(animalChoices)){
        animalButtons[animal] = {label : animal, callback: () => animalChoice = animal}
    }

    new Dialog({
        title: "Select your animal",
        buttons:  animalButtons,
        close: html => {
            toggleNaturalWeapons(keyword, animalChoice, animalChoices[animalChoice])
        },
        default: "Cancel"
    }).render(true);
}

async function toggleNaturalWeapons(keyword, animalName, naturalWeapons){
    let fist_img = "systems/pf2e/icons/equipment/weapons/fist.jpg"
    //Create only the items that we need
    let weaponAbility = {"label": "Offensive Ability", "type":"String", "value":"str"}
    for (let naturalWeapon of naturalWeapons){
        // Add -Animal Instinct- to the name will make for easier deletion later
        let naturalWeaponName = `${keyword} ${animalName} ${naturalWeapon.name}`
        let naturalWeaponItem = {
            "type":"weapon", "effects":[], "name":naturalWeaponName, img:fist_img,
            "data.weaponType.value" : "unarmed", "data.group":"brawling", "data.ability":weaponAbility,
            "data.damage.damageType":naturalWeapon.damageType, "data.damage.die": naturalWeapon.die,
            "data.traits.label": "Weapon Traits", "data.traits.value": naturalWeapon.traits
        }
        await actor.createEmbeddedEntity("OwnedItem", naturalWeaponItem)
    }
}


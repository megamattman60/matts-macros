// check for actor
if (!actor) {
    ui.notifications.warn("You must have an actor selected.");
	
} else {

	let equippedWeapons = {};
	let selectedWeapon = "";

	(async () => {
		for (let token of canvas.tokens.controlled) {
			//find all weapon items that the actor is holding
			const weapon = token.actor.data.items.filter(item => item.type === 'weapon');
			console.log(weapon);

            // for each weapon create a button object for the dialog box we are going to me later			
			weapon.forEach(item => {
				//Create entry to be passed to dialog buttons
				equippedWeapons[item.name] = {label : item.name, callback: () => selectedWeapon = item}
				console.log(item.name)
		  });
		}
		// Render the dialog box
		// Button: cosumes the list of buttons objects that we made earlier
		new Dialog({
			title: "Select Weapon to dis/enchant",    
			buttons:  equippedWeapons
			,
			default: "Cancel",
			close: html => {
				updateWeapon(selectedWeapon)
			}
		}).render(true);

	})();

async function updateWeapon(item) {
	  if (!item) {
		  return false
	  }
	  //Create various entries for the rules - this makes things easier to modify ;)
	  let mwAttackBonusRuleLabel = "Magic Weapon Attack  Item Bonus";
	  let mwStrikingRuleLabel = "Magic Weapon Striking";
	
	  //Setting the selector to use the item_id will mean teh rule will only apply to single item
	  let mwAttackBonusSelect = item._id + "-attack"
	  let mwStrikeSelector = item._id + "-damage"
	
	  //Use the item name here so that it nicer to read on the character sheet
	  let mwEffectName = "Effect: Magic Weapon " + item.name;
	
	  //Create the rules to be applied to the effect - not the use of info pulled from item.data
	  let mwStrikingRule = {"key":"PF2E.RuleElement.DamageDice", "label":mwStrikingRuleLabel,"value":"1", "dieSize":item.data.damage.die, "traits":["magical"], "selector":mwStrikeSelector, "value":{"brackets":[{"value":{"diceNumber":1}}]}}	  
	  let mwAttackBonusRule = {"key":"PF2E.RuleElement.FlatModifier","label":mwAttackBonusRuleLabel,"value":"1", "selector":mwAttackBonusSelect, "type": "item"}
	 
	  
	  // This is the on off switch if Magic weapon effect already applied it is removed
      let mwPresent = actor.items.find(i=>i.name===mwEffectName)	 
	  if (mwPresent){
		  mwPresent.delete()
	  } else {
		  let mwRules = [mwAttackBonusRule]
		  // if there is already a striking rune or the number of damage dice doens't = 1 then do not add the striking rule
		  if (item.data.strikingRune.value == "" && item.data.damage.dice == "1") {
			    mwRules.push(mwStrikingRule);			
		  }
		  
		  //Create a new effect item
		  let mwItemEffect = {"data.rules": mwRules, effects:[], name:mwEffectName, type:"effect"}
		  // Create the effect
		  await actor.createOwnedItem(mwItemEffect)
		  
	  } 
	      //toggle the magic weapon image
          for (let token of canvas.tokens.controlled) {
            token.toggleEffect("systems/pf2e/icons/spells/magic-weapon.jpg");
          }
	}
}
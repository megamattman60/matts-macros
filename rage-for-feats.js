/*
  This implementation expects:
  the instinct class features to have toggleRule element ac and damage modifiers
  giant instinct has set the oversized toggle - this is currently player responsability

*/
(async () => {
    if (!actor) {
        ui.notifications.warn("Please select a token");
        return false;
    }

    let functionMap = {
        "Animal Instinct" : applyBestialInstinct,
        "Giant Instinct" : applyGiantInstinct,
        "Spirit Instinct" : applySpiritInstinct,
        "Dragon Instinct" : applyDragonInstinct,
        "Fury Instinct" : applyRageEffect
    }

    //If an instinct feat is present then
    let hasRageInstinct = actor.data.items.filter(item => item.type === "feat").find(feat => feat.name.includes("Instinct"));
    if (hasRageInstinct) {
        console.log(`LOOKS LIKE YOU HAVE: ${hasRageInstinct.name}`)
        await functionMap[hasRageInstinct.name]()
    }
})();

// remove rage effect and toggle off rage bonus and taken effect
async function removeRageEffect () {
    let hasRageEffect = actor.items.find(item => item.type === "effect" && item.name.includes("Effect: Rage"));
    if (hasRageEffect){
        console.log(hasRageEffect);
        hasRageEffect.delete();
        if (token.data.effects.includes("systems/pf2e/icons/features/classes/rage.jpg")) {
            token.toggleEffect("systems/pf2e/icons/features/classes/rage.jpg");
        }
        return true
    }
    console.log("NO RAGE FOUND");
    return false
}

async function applyRageEffect(){
    let ragePresent = await removeRageEffect()
    console.log(ragePresent)
    if (!ragePresent){
        if (!token.data.effects.includes("systems/pf2e/icons/features/classes/rage.jpg")) {
            token.toggleEffect("systems/pf2e/icons/features/classes/rage.jpg");
        }
        let rageEffect = game.items.getName("Effect: Rage").data
        console.log(rageEffect)
        await actor.createOwnedItem(rageEffect)
    }
}

async function applySpiritInstinct(){
    //TODO:- add damageType to damage rule in class feature based on user choice
    applyRageEffect();

}

async function applyDragonInstinct(){
    //TODO:- add damageType to damage rule in class feature based on user choice
    applyRageEffect();

}

async function applyGiantInstinct(){
    if (!removeRageEffect()){
        //TODO wait for condition rule to be implemented and add it to giant instinct
        applyRageEffect();
        if (!item){
            ui.notifications.warn("Apply clumsy 1 if wielding obversized weapon")
        }
}

async function applyBestialInstinct(){
    //check if rage already present - remove natural weapons and then exit
    let keyword = "-Animal Instinct-"
    if (removeRageEffect()){
        let foundNaturalWeapons = token.actor.data.items.filter(item => item.type === 'weapon').filter(weapon => weapon.name.includes(keyword));
        if (foundNaturalWeapons.length){
            foundNaturalWeapons.forEach(foundNaturalWeapon=>actor.deleteOwnedItem(foundNaturalWeapon._id))
            return true
        }
        return true
    }

    //apply rage effect
    let rageEffect = game.items.getName("Effect: Rage Animal Instinct").data
    await actor.createOwnedItem(rageEffect)
    //Ask player their animal
    let animalChoices = {
        "Ape": [{"name":"Fist", "die":"d10", "damageType":"B", "traits": ["Grapple", "unarmed"]}],
        "Bear":[{"name":"Jaws", "die": "d10", "damageType":"piercing", "traits":[" Unarmed"]},
                {"name":"Claw", "die": "d6", "damageType": "slashing","Traits":[" Agile", "unarmed"]}],
        "Frog":[{"name":"Jaws", "die": "d10", "damageType":"B", "traits":[" Unarmed"]},
                {"name":"Tongue", "die": "d4", "damageType": "B","Traits":[" Agile", "unarmed"]}],
        "Cat":[{"name":"Jaws", "die": "d10", "damageType":"piercing", "traits":[" Unarmed"]},
                {"name":"Claw", "die": "d6", "damageType": "slashing","Traits":[" Agile", "unarmed"]}],
        "Bull": [{"name":"Horn", "die":"d10", "damageType":"piercing", "traits": ["Shove", "unarmed"]}],
        "Deer": [{"name":"Antler", "die":"d10", "damageType":"piercing", "traits": ["Grapple", "unarmed"]}],
        "Shark": [{"name":"Jaws", "die":"d10", "damageType":"piercing", "traits": ["Grapple", "unarmed"]}],
        "Snake": [{"name":"Fangs", "die":"d10", "damageType":"piercing", "traits": ["Grapple", "unarmed"]}],
        "Wolf": [{"name":"Fangs", "die":"d10", "damageType":"piercing", "traits": ["Trip", "unarmed"]}]
    }
    let animalChoice = ""
    let animalButtons = {}
    for (let animal of Object.keys(animalChoices)){
        animalButtons[animal] = {label : animal, callback: () => animalChoice = animal}
    }

    new Dialog({
        title: "Select your animal",
        buttons:  animalButtons,
        close: html => {
            toggleNaturalWeapons(keyword, animalChoice, animalChoices[animalChoice])
        },
        default: "Cancel"
    }).render(true);
}

async function toggleNaturalWeapons(keyword, animalName, naturalWeapons){
    let fist_img = "systems/pf2e/icons/equipment/weapons/fist.jpg"
    //Create only the items that we need
    let weaponAbility = {"label": "Offensive Ability", "type":"String", "value":"str"}
    for (let naturalWeapon of naturalWeapons){
        // Add -Animal Instinct- to the name will make for easier deletion later
        let naturalWeaponName = `${keyword} ${animalName} ${naturalWeapon.name}`
        let naturalWeaponItem = {
            "type":"weapon", "effects":[], "name":naturalWeaponName, img:fist_img,
            "data.weaponType.value" : "unarmed", "data.group":"brawling", "data.ability":weaponAbility,
            "data.damage.damageType":naturalWeapon.damageType, "data.damage.die": naturalWeapon.die,
            "data.traits.label": "Weapon Traits", "data.traits.value": naturalWeapon.traits
        }
        await actor.createEmbeddedEntity("OwnedItem", naturalWeaponItem)
    }
}

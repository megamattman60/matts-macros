(async () => {
    if (!actor) {
        ui.notifications.warn("Please select a token");
        return false;
    }

    let ragePresent = actor.items.find(item => item.type === "effect" && item.name.includes("Effect: Rage"));
    console.log(ragePresent)
    if (!ragePresent){
        let rageEffect = game.items.getName("Effect: Rage").data
        await actor.createOwnedItem(rageEffect)
    }
    else {
        ragePresent.delete()
    }

})();